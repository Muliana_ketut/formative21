package com.code21.code21.controller;

import java.util.List;

import com.code21.code21.model.Zodiac;
import com.code21.code21.repository.ZodiacRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// import jdk.javadoc.internal.doclets.formats.html.SourceToHTMLConverter;

@CrossOrigin(origins = "http://localhost:3000")
@RestController
@RequestMapping("/api/")
public class ZodiacController{
    @Autowired
    private ZodiacRepository repoZodi;

    // create users rest api
    @PostMapping(value="/users", consumes="application/json")
    public Zodiac createEmployee(@RequestBody Zodiac zodiac) {
        System.out.println(zodiac.getJeniszodiac());
        return repoZodi.save(zodiac);
    }
}