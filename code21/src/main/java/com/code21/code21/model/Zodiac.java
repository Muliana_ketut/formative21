package com.code21.code21.model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Zodiac {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String name;

    @JsonFormat(pattern="yyyy-MM-dd")
    private Date birthdate;

    private String jeniszodiac;

    public Zodiac() {
    }

    public Zodiac(int id, String name, Date birthdate, String jeniszodiac) {
        this.id = id;
        this.name = name;
        this.birthdate = birthdate;
        this.jeniszodiac = jeniszodiac;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getJeniszodiac() {
        return jeniszodiac;
    }

    public void setJeniszodiac(String jeniszodiac) {
        this.jeniszodiac = jeniszodiac;
    }

    
}
