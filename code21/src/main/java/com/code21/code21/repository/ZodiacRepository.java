package com.code21.code21.repository;

import com.code21.code21.model.Zodiac;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ZodiacRepository extends JpaRepository<Zodiac, Integer>{
    
}
