package com.code21.code21;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Code21Application {

	public static void main(String[] args) {
		SpringApplication.run(Code21Application.class, args);
	}

}
