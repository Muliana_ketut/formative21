import React from "react";

import "./HelloWorld.css";

class HelloWorld extends React.Component {
  state = { date: new Date() };

  render() {
    return (
      <div class="container">
        <h1>Hello World</h1>
        <p>{this.state.date.toLocaleDateString()}</p>
      </div>
    );
  }
}

export default HelloWorld;
