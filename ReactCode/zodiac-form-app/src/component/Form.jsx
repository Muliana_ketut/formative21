import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import "./Form.css";

const Input = (props) => {
  return (
    <div className="form-group">
      <input
        className="form-control"
        type={props.type}
        id={props.id}
        placeholder={props.placeholder}
        required
      />
      <br />
    </div>
  );
};

// Button Component
const Button = () => {
  return (
    <button type="submit" className="btn btn-primary" onClick={Post}>
      Submit
    </button>
  );
};

// Form component
const Form = () => {
  return (
    <div className="container card">
      <div className="card-header">
        <h1>Check Your Zodiac</h1>
        <hr></hr>
        <div className="card-body">
          <h3>Name</h3>
          <Input type="text" id="name" placeholder="Input Name..." />
          <h3>Birthday</h3>
          <Input type="date" id="birthdate" />
          <h3>Zodiac</h3>
          <input
            type="text"
            id="zodiac"
            className="form-control"
            disabled
          ></input>
          <br />
          <Button />
        </div>
      </div>
    </div>
  );
};

function Post() {
  let birthdate = document.getElementById("birthdate").value;
  let birthmonth = parseInt(birthdate.substring(5, 7));
  let birthday = parseInt(birthdate.substring(8, 10));
  let res;
  if (
    (birthmonth === 1 && birthday >= 20) ||
    (birthmonth === 2 && birthday <= 18)
  ) {
    res = "Aquarius";
  }

  if (
    (birthmonth === 2 && birthday >= 19) ||
    (birthmonth === 3 && birthday <= 20)
  ) {
    res = "Pisces";
  }
  if (
    (birthmonth === 3 && birthday >= 21) ||
    (birthmonth === 4 && birthday <= 19)
  ) {
    res = "Aries";
  }

  if (
    (birthmonth === 4 && birthday >= 20) ||
    (birthmonth === 5 && birthday <= 20)
  ) {
    res = "Taurus";
  }

  if (
    (birthmonth === 5 && birthday >= 21) ||
    (birthmonth === 6 && birthday <= 20)
  ) {
    res = "Gemini";
  }
  if (
    (birthmonth === 6 && birthday >= 21) ||
    (birthmonth === 7 && birthday <= 22)
  ) {
    res = "Cancer";
  }
  if (
    (birthmonth === 7 && birthday >= 23) ||
    (birthmonth === 8 && birthday <= 22)
  ) {
    res = "Leo";
  }
  if (
    (birthmonth === 8 && birthday >= 23) ||
    (birthmonth === 9 && birthday <= 22)
  ) {
    res = "Virgo";
  }
  if (
    (birthmonth === 9 && birthday >= 23) ||
    (birthmonth === 10 && birthday <= 22)
  ) {
    res = "Libra";
  }
  if (
    (birthmonth === 10 && birthday >= 23) ||
    (birthmonth === 11 && birthday <= 21)
  ) {
    res = "Scorpio";
  }
  if (
    (birthmonth === 11 && birthday >= 22) ||
    (birthmonth === 12 && birthday <= 21)
  ) {
    res = "Sagittarius";
  }
  if (
    (birthmonth === 12 && birthday >= 22) ||
    (birthmonth === 1 && birthday <= 19)
  ) {
    res = "Capricorn";
  }
  document.getElementById("zodiac").value = res;

  // POST request using fetch()
  let data = {
    name: document.getElementById("name").value,
    birthdate: document.getElementById("birthdate").value,
    jeniszodiac: res,
  };
  console.log(data);

  fetch("http://localhost:8080/api/users", {
    // Adding method type
    method: "POST",

    // Adding body or contents to send
    body: JSON.stringify(data),

    // Adding headers to the request
    headers: {
      "Content-type": "application/json; charset=UTF-8",
    },
  });
  alert("Zodiac Anda "+res);
}
export default Form;
